class Tail:
	def __init__(self, log_file):
		self.f = open(log_file, encoding='utf-8', errors='ignore')

	def __iter__(self):
		return self

	def __next__(self):
		line = self.f.readline()
		if line:
			return line
		else:
			raise StopIteration
