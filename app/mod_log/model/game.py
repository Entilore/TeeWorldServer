from app.mod_log.model.event import Event
from app.mod_log.model.player import Player
from app.mod_log.model.round import Round
from app.util import check_type


class Game:
	def __init__(self, name=None):
		self._rounds = []
		self._players = {}
		self._name = name

	@property
	def name(self):
		return self._name

	@staticmethod
	def __get_idx(key):
		return key - 1

	def start_round(self, date=None):
		# if round is empty, reuse the same
		if self._rounds:
			r = self.get_current_round()
			if r:
				self._rounds.append(Round(date))
			else:
				r.date = date
		else:
			self._rounds.append(Round(date))

	def __len__(self):
		return len(self._rounds)

	def __getitem__(self, key):
		idx = Game.__get_idx(key)
		print(idx)
		print(len(self._rounds))
		return self._rounds[idx]

	def __setitem__(self, key, value):
		idx = Game.__get_idx(key)
		self._rounds[idx] = value

	def __contains__(self, item):
		return Game.__get_idx(item) < len(self._rounds)

	def add_player(self, player):
		if isinstance(player, str):
			player_name = player
			player = Player(player, self)
		else:
			player_name = player.name

		if player_name not in self._players:
			self._players[player_name] = player
		return player

	def get_or_add_player(self, player_name):
		p = self.get_player(player_name)
		if not p:
			p = self.add_player(player_name)
		return p

	def get_player(self, player_name):
		if player_name in self._players:
			return self._players[player_name]
		return None

	def get_current_round(self):
		if not self._rounds:
			self.start_round()
		return self._rounds[-1]

	def add_event(self, event):
		check_type(event, Event)

		assert event.owner.game is self

		r = self.get_current_round()
		r.append(event)

	def __retrieve_events(self, functor, key):
		events = {}
		for r in self._rounds:
			r.retrieve_events(events, functor, key)

		return events

	def get_all_kills(self, player):
		return self.__retrieve_events(
			lambda e: isinstance(e, Event) and e.killer == player,
			lambda e: e.victim
		)

	def get_all_killers(self, player):
		return self.__retrieve_events(
			lambda e: isinstance(e, Event) and e.victim == player,
			lambda e: e.killer
		)

	def get_kills_at_round(self, player, round_id=-1):
		events = {}
		if round_id < 0:
			round = self.get_current_round()
		else:
			round = self[round_id]

		round.retrieve_events(
			events,
			lambda e: isinstance(e, Event) and e.killer == player,
			lambda e: e.victim
		)
		return events

	def get_killers_at_round(self, player, round_id=-1):
		events = {}
		if round_id < 0:
			round = self.get_current_round()
		else:
			round = self[round_id]

		round.retrieve_events(
			events,
			lambda e: isinstance(e, Event) and e.victim == player,
			lambda e: e.killer
		)
		return events

	def get_all_rounds(self):
		return self._rounds

	def get_smallest_id(self):
		for i in range(len(self._rounds)):
			if i in self:
				return i

	def get_biggest_id(self):
		for i in reversed(range(len(self._rounds)+1)):
			if i in self:
				return i

