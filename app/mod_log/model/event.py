import datetime

from app.mod_log.model.player import Player
from app.mod_log.model.weapon import Weapon
from app.util import check_type


class Event:
	def __init__(self, owner, date):
		check_type(owner, Player)
		check_type(date, datetime.datetime)
		self._owner = owner
		self._date = date

	@property
	def owner(self):
		return self._owner

	@property
	def date(self):
		return self._date


class Kill(Event):
	"""kill killer='3:Lorantmuki' victim='1:nachoteman' weapon=3 special=0"""

	def __init__(self, killer, victim, weapon, special, date):
		super().__init__(killer, date)
		check_type(killer, Player)
		check_type(victim, Player)
		check_type(weapon, Weapon)

		self._victim = victim
		self._weapon = weapon
		self._special = special

	@property
	def killer(self):
		return self._owner

	@property
	def victim(self):
		return self._victim

	@property
	def weapon(self):
		return self._weapon
