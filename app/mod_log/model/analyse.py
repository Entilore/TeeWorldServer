#!/usr/bin/env python
# coding: utf8

import re

from app import app
from app.mod_log.model.event import Kill
from app.mod_log.model.tail import Tail
from app.mod_log.model.game import Game
from app.mod_log.model.weapon import Weapon
from datetime import datetime

log_file = Tail(app.config["LOG_FILE_PATH"])

game = Game()

extract_info = re.compile(r'^\[(.*?)\]\[game\]:\s*(start round|.*?)\s(.*?)\s*$')
extract_player = re.compile(r"^'*\d*:*(.*?)'*$")


def shall_be_a_player(name):
	player_argument = ["killer", "victim", "player"]
	return name in player_argument


def parse_argument(args):
	l = args.split()
	output = {}
	last = None
	for itm in l:
		if "=" in itm:
			name, value = itm.split("=")
			if shall_be_a_player(name):
				value = extract_player.match(value).group(1)
			output[name] = value
			last = name
		else:
			itm = itm.replace("'", "")
			output[last] += " " + itm
	return output


def kill(killer, victim, weapon, special, date):
	weapon = int(weapon)
	killer_object = game.get_or_add_player(killer)
	victim_object = game.get_or_add_player(victim)

	try:
		weapon_object = Weapon(weapon)
	except ValueError:
		# unexisting weapon
		return

	e = Kill(killer_object, victim_object, weapon_object, special, date)

	game.add_event(e)


def analyse_line(line):
	global game

	r = extract_info.match(line)
	if not r:
		return

	d = datetime.fromtimestamp(int(r.group(1), 16))
	action = r.group(2)
	param = parse_argument(r.group(3))
	param["date"] = d
	if action == "kill":
		kill(**param)
	elif action == "start round":
		game.start_round(d)
	else:
		pass


def update():
	for line in log_file:
		analyse_line(line)
