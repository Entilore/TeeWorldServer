from app.util import Flyweight


@Flyweight
class Player:
	def __init__(self, name, game=None, ip=None):
		self._name = name
		self._ip = ip
		self._game = game

	@property
	def game(self):
		return self._game

	@game.setter
	def game(self, value):
		self._game = value

	@property
	def name(self):
		return self._name

	def get_all_kills(self):
		assert self._game is not None
		return self._game.get_all_kills(self)

	def get_all_killers(self):
		assert self._game is not None
		return self._game.get_all_killers(self)

	def get_kills_at_round(self, round=-1):
		return self._game.get_kills_at_round(self, round)

	def get_killers_at_round(self, round=-1):
		return self._game.get_killers_at_round(self, round)

	def __str__(self):
		return self._name
