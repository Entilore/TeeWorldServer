from collections import defaultdict

from datetime import datetime

from app import app
from app.mod_log.model.event import Event, Kill
from app.util import check_type


class Round:
	def __init__(self, date=None):
		self._events = []
		self._date = date

	@property
	def date(self):
		return self._date

	@date.setter
	def date(self, value):
		if self._events and value > self._date:
			raise ValueError("{} is posterior than the first event of this round")
		self._date = value

	@staticmethod
	def __get_idx(key):
		return key

	def __getitem__(self, key):
		idx = self.__get_idx(key)
		return self._events[idx]

	def __setitem__(self, key, value):
		idx = self.__get_idx(key)
		self._events[idx] = value

	def __append(self, value):
		check_type(value, Event)
		date_event = value.date
		if not self._date:
			self._date = date_event
		self._events.append(value)

	def append(self, value):
		self.__append(value)

	def __add__(self, value):
		self.__append(value)
		return self

	def __contains__(self, value):
		return value in self._events

	def __len__(self):
		return len(self._events)

	def retrieve_events(self, dictionary, condition, key):
		for e in (e for e in self._events if condition(e)):
			k = key(e)
			if k in dictionary:
				dictionary[k] += 1
			else:
				dictionary[k] = 1

	def get_result(self):
		kills = defaultdict(int)
		deaths = defaultdict(int)
		points = defaultdict(int)
		gen_kill = (event for event in self._events if isinstance(event, Kill))
		for kill in gen_kill:
			player = kill.owner
			victim = kill.victim
			if player is not victim:
				kills[player] += 1
				points[player] += 1
			else:
				points[player] -= 1
			points[victim] = points[victim]
			deaths[victim] += 1

		return kills, deaths, points

	def get_winner(self):
		_, _, points = self.get_result()
		if points:
			return max(points, key=points.get)
		else:
			return None

	def is_ended(self):
		game_time = (app.config["ROUND_TIME"]*60)
		return (datetime.now() - self.date).total_seconds() > game_time
