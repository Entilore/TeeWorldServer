from enum import Enum
from app.util import Flyweight


@Flyweight
class Weapon(Enum):
	NO_WEAPON = -1
	HAMMER = 0 # V
	GUN = 1 # V
	SHOTGUN = 2# V
	GRENADE_LAUNCHER = 3 # V
	RIFLE = 4 # NINJA
	KATANA = 5

	def __str__(self):
		name = self.name.lower().replace("_", " ")
		return name
