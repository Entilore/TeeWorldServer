#!/usr/bin/env python

# Import flask dependencies
from flask import Blueprint, request, render_template, flash, g, session, redirect, url_for, abort

import app
from app.mod_log.model import analyse
# Define the blueprint: 'auth', set its url prefix: app.url/auth

mod_log = Blueprint('log', __name__)


# Set the route and accepted methods


def __compute(player_name, f):
	analyse.update()
	res = f(player_name)
	res = res.replace("\n", "<br>")
	res = res.replace("\t", "&nbsp;")
	return res


@mod_log.context_processor
def inject_srv_name():
	return dict(teeworlds_srv_name=app.name)


@mod_log.route("/<player_name>")
def get_player(player_name):
	analyse.update()
	p = analyse.game.get_player(player_name)
	if not p:
		abort(404)
	kills = p.get_all_kills()
	killers = p.get_all_killers()
	return render_template("player.html", player=p, kills=kills, killers=killers)


@mod_log.route("/round/<int:round_id>/<player_name>")
def get_kills_at_round(player_name, round_id):
	analyse.update()
	p = analyse.game.get_player(player_name)

	if not p:
		abort(404)

	kills = p.get_kills_at_round(round_id)
	killers = p.get_killers_at_round(round_id)
	return render_template("player.html", round=round_id, player=p, kills=kills, killers=killers)


@mod_log.route("/")
@mod_log.route("/rounds")
def get_all_rounds():
	analyse.update()
	rounds = analyse.game.get_all_rounds()
	return render_template("rounds.html", rounds=rounds)


@mod_log.route("/round/<int:round_id>")
def get_round(round_id):
	analyse.update()
	if round_id not in analyse.game:
		abort(404)
	round = analyse.game[round_id]
	first_idx = analyse.game.get_smallest_id()
	last_idx = analyse.game.get_biggest_id()
	return render_template("round.html", round=round, id=round_id, first_idx=first_idx, last_idx=last_idx)
