# filters.py
from babel import dates
import jinja2
from flask import Blueprint, url_for

from app.mod_log.model.event import Kill
from app.mod_log.model.player import Player

filters = Blueprint('filters', __name__)


# using the decorator
@filters.app_template_filter("datetime")
def format_datetime(value, date_format='full'):
	if date_format == 'full':
		date_format = "EEEE, d. MMMM y 'at' HH:mm"
	elif date_format == 'medium':
		date_format = "EE dd.MM.y HH:mm"
	return dates.format_datetime(value, date_format)


@filters.app_template_filter("filter_type")
def format_datetime(iterable, tp=Kill):
	for r in iterable:
		print(r.weapon)
	return (r for r in iterable if isinstance(r, tp))


@filters.app_template_filter("previous")
def get_url_for(id):
	return id - 1


@filters.app_template_filter("get_url_for")
def get_url_for(*args):
	def get_resource_name(resource):
		if isinstance(resource, Player):
			return "player_name"
		elif isinstance(resource, int):
			return "round_id"


	kargs = {get_resource_name(v): v for v in args if (v or v is 0)}

	if len(kargs) == 1:
		v = next(iter(kargs.keys()))
		if v is "player_name":
			url = "log.get_player"
		elif v is "round_id":
			url = "log.get_round"

	elif len(args) == 2:
		url = "log.get_kills_at_round"

	return url_for(url, **kargs)


@filters.app_template_filter("plural")
def get_plural(l):
	if len(l) > 1:
		return "s"

	return ""


@filters.app_template_filter("format")
def get_format(s, **kargs):
	return s.format(**kargs)