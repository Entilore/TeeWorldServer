def check_type(obt, typ):
	if not isinstance(obt, typ):
		raise TypeError("{} is not an instance of {}".format(obt, typ.__name__))


class Flyweight(object):
	def __init__(self, cls):
		self.__dict__.update({'instances': {}, 'cls': cls})

	def __call__(self, id, *args, **kwargs):
		try:
			return self.instances[id]
		except KeyError:
			instance = self.cls(id, *args, **kwargs)
			self.instances[id] = instance
			return instance

	def __getattr__(self, attr):
		return getattr(self.cls, attr)

	def __setattr__(self, attr, value):
		setattr(self.cls, attr, value)

	def __instancecheck__(self, other):
		return isinstance(other, self.cls)
