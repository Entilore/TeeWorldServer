#!/usr/bin/env python

# Import flask and template operators
from flask import Flask, send_from_directory, request
from flask_bootstrap import Bootstrap

# Define the WSGI application object
from app.filters import filters

app = Flask(__name__)



Bootstrap(app)

# Configurations
app.config.from_object('config')

# Import a module / component using its blueprint handler variable (mod_log)
from app.mod_log.controllers import mod_log as log_module

# Register blueprint(s)
app.register_blueprint(log_module)
app.register_blueprint(filters)


# app.register_blueprint(xyz_module)
# ..

def _get_server_name():
	if "CONF_FILE_PATH" in app.config:
		conf_file = app.config["CONF_FILE_PATH"]
		with open(conf_file) as f:
			for line in f:
				words = line.split()
				key = words.pop(0)
				if key == "sv_name":
					return " ".join(words)


name = _get_server_name()


@app.route('/robots.txt')
def static_from_root():
	return send_from_directory(app.static_folder, request.path[1:])


@app.route('/favicon.ico')
def static_favicon():
	return send_from_directory(app.static_folder, request.path[1:])


@app.route('/style/<path:path>')
def send_js(path):
	return send_from_directory('static', path)
