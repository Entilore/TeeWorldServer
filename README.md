### Teeworlds server

Simple server to display stats info of a TeeWorlds server

The info are computed from the output of the teeworlds server. 

A running example can be found [here](http://teeworlds.hareau.eu)