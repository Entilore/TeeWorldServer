"""
Check whether the external IP as changed, and send it per email
"""
from setuptools import find_packages, setup

dependencies = ['flask', 'flask-bootstrap', 'babel']

setup(
    name='app',
    version='0.1.5',
    url='https://gitlab.com:Entilore/TeeWorldServer',
    license='BSD',
    author='Thomas Hareau',
    author_email='thomas@hareau.eu',
    description='Analyse the log files of teeworlds server, and provides an API to get different values',
    long_description=__doc__,
    packages=find_packages(exclude=[]),
    include_package_data=True,
    zip_safe=False,
    platforms='any',
    install_requires=dependencies,
    classifiers=[
        # As from http://pypi.python.org/pypi?%3Aaction=list_classifiers
        # 'Development Status :: 1 - Planning',
        # 'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        # 'Development Status :: 4 - Beta',
        # 'Development Status :: 5 - Production/Stable',
        # 'Development Status :: 6 - Mature',
        # 'Development Status :: 7 - Inactive',
        'Framework :: Flask',
        'Environment :: Web Environment',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: BSD License',
        'Operating System :: POSIX',
        'Operating System :: MacOS',
        'Operating System :: Unix',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Topic :: Multimedia :: Graphics :: Viewers',
    ]
)

